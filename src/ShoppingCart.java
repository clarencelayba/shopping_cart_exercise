import java.util.*;

public class ShoppingCart {

    HashMap<String, Product> products;
    ArrayList<Product> addedProducts;
    ArrayList<Promo> promos;
    boolean isPromoCodeActivated;

    double deduction;
    ArrayList<Product> freeProducts;

    public ShoppingCart(HashMap<String, Product> products, ArrayList<Promo> promos) {
        this.products = products;
        this.promos = promos;
        addedProducts = new ArrayList<Product>();
        this.isPromoCodeActivated = false;
    }

    public void add(String productCode) {
        if (products.get(productCode) != null) addedProducts.add(products.get(productCode));
    }

    public void add(String productCode, String promoCode) {
        if (products.get(productCode) != null) addedProducts.add(products.get(productCode));
        if (promoCode.equals("I<3AMAYSIM")) isPromoCodeActivated = true;
    }

    public double total() {
        if (freeProducts == null) executePromo();

        double total = addedProducts.stream().mapToDouble(product -> product.price).sum();
        double totalResult = total - deduction;

        if (this.isPromoCodeActivated){
            System.out.println("Promo Code Applied");
            return roundOff(totalResult - (totalResult * .10));
        }  else return roundOff(totalResult);
    }

    private void executePromo() {
        deduction = 0;
        freeProducts = new ArrayList<Product>();
        for (int i = 0; i < promos.size(); i++) {
            PromoResult promoResult = promos.get(i).getResult(addedProducts);
            deduction += promoResult.totalDeduction;
            freeProducts.addAll(promoResult.totalFreeProducts);
        }
        if(!freeProducts.isEmpty()){
            System.out.println("Free Products: ");
            freeProducts.forEach(a -> System.out.println(a.name));
        }
    }

    public ArrayList<Product> items() {
        if (freeProducts == null) executePromo();
        ArrayList<Product> result = addedProducts;
        //result.addAll(freeProducts);
        return result;
    }
    public ArrayList<Product> totalItems() {
        ArrayList<Product> result = addedProducts;
        result.addAll(freeProducts);
        return result;
    }
    private double roundOff(double value) {
        return Math.round(value * 100.0) / 100.0;
    }
}