import java.util.*;

public class PromoResult {
    ArrayList<Product> totalFreeProducts;
    double totalDeduction;

    public PromoResult(ArrayList<Product> totalFreeProducts, double totalDeduction) {
        this.totalFreeProducts = totalFreeProducts;
        this.totalDeduction = totalDeduction;
    }
}