public class Product {
    String code;
    String name;
    double price;

    public Product(String code, String name, double price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }
}
