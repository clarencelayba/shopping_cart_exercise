import java.util.*;

public class CartExercise {

    public static void main(String[] args) {
        HashMap<String, Product> products = new HashMap<String, Product>();
        products.put("ult_small", new Product("ult_small", "Unlimited 1GB", 24.90));
        products.put("ult_medium", new Product("ult_medium", "Unlimited 2GB", 29.90));
        products.put("ult_large", new Product("ult_large", "Unlimited 5GB", 44.90));
        products.put("1gb", new Product("1gb", "1 GB Data-pack", 9.90));

        ArrayList<Promo> promos = new ArrayList<Promo>();

        // -----------------
        Promo promoA = new Promo(
                products.get("ult_small"), //targetProduct
                new ArrayList<Product>(), //freeProducts
                .333333, // discount : 100% = 1, 50% = .5
                3,  //numericCondition
                false); //isModulus
        promos.add(promoA);

        // -----------------
        Promo promoB = new Promo(
                products.get("ult_large"), //targetProduct
                new ArrayList<Product>(), //freeProducts
                .11135857461024, // discount : 100% = 1, 50% = .5
                3,  //numericCondition
                false); //isModulus
        promos.add(promoB);

        // ------------------
        ArrayList<Product> freeProductsPromoC = new ArrayList<Product>();
        freeProductsPromoC.add(products.get("1gb"));

        Promo promoC = new Promo(
                products.get("ult_medium"), //targetProduct
                freeProductsPromoC, //freeProducts
                0, // discount : 100% = 1, 50% = .5
                1,  //numericCondition
                true); //isModulus
        promos.add(promoC);


        ShoppingCart cart = new ShoppingCart(products, promos);
        // PromoA (Scenario 1) --------------------------------------------
        cart.add("ult_small");
        cart.add("ult_small");
        cart.add("ult_small");
        cart.add("ult_large");

        // PromoB (Scenario 2) --------------------------------------------
        //cart.add("ult_small");
        //cart.add("ult_small");
        //cart.add("ult_large");
        //cart.add("ult_large");
        //cart.add("ult_large");
        //cart.add("ult_large");

        // PromoC (Scenario 3) --------------------------------------------
        //cart.add("ult_small");
        //cart.add("ult_medium");
        //cart.add("ult_medium");

        // With PromoCode (Scenario 4) ------------------------------------
        //cart.add("ult_small");
        //cart.add("1gb", "I<3AMAYSIM");

        System.out.println("\nCart Total: $" + cart.total());
        System.out.println("\nItems Added:");
        cart.items().forEach(prod -> System.out.println(prod.name));
        System.out.println("\nItems on the Cart:");
        cart.totalItems().forEach(totalItems -> System.out.println(totalItems.name));
    }
}