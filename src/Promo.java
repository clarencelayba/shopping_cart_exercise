import java.util.*;
import java.util.stream.*;

public class Promo {
    Product targetProduct;

    ArrayList<Product> freeProducts;
    double discount;

    boolean isDivisible;
    int numericCondition;


    public Promo(Product targetProduct, ArrayList<Product> freeProducts,
                           double discount, int numericCondition, boolean isDivisible) {
        this.targetProduct = targetProduct;
        this.freeProducts = freeProducts;
        this.discount = discount;
        this.numericCondition = numericCondition;
        this.isDivisible = isDivisible;
    }

    public PromoResult getResult(ArrayList<Product> addedProducts) {
        double totalDeduction = 0;
        ArrayList<Product> totalFreeProducts = new ArrayList<Product>();
        int targetProductSize = addedProducts.stream().filter(p -> p.code == targetProduct.code).collect(Collectors.toList()).size();
        if (isDivisible) {
            int multiplier = targetProductSize / numericCondition;
            for (int i = 0; i < multiplier; i++) {
                totalDeduction = (discount * targetProduct.price) * targetProductSize;
                totalFreeProducts.addAll(freeProducts);
            }
            return new PromoResult(totalFreeProducts, totalDeduction);
        } else {
            if (targetProductSize >= numericCondition) {
                totalDeduction = (discount * targetProduct.price) * targetProductSize;
                totalFreeProducts.addAll(freeProducts);
            }
        }
        return new PromoResult(totalFreeProducts, totalDeduction);
    }
}